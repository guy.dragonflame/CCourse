/*
 Answer to HW1 Q2: Get a string of up to 80 chars and replace a sequence of case
 sensitive letters with "-".
  I assume all the data is valid (as instructed in the question).
 */

#include <stdio.h>
#include <string.h>
#define STR_SIZE 82 /*82 to allow for 80 chars +\n + \0. the \n will be replaced by \0 so string will be 80*/
#define TRUE 1

void abc(char *str){
    char *i = str; /* the pointer to go over all the string and find sequences*/ 
    for(; *i; ){
    	/* check if char is the next char in ASCII and a letter */
        char *initPlace = i; /*save the initial place of i*/
        for (; *(i+1) && *(i+1) == *i + 1 && ((*i >= 'a' && *i < 'z') || (*i >= 'A' && *i < 'Z')); i++){
            ;
        }
        /* a sequence of more than 2 - replace with '-' */
        if (i - initPlace > 1){
            *str++ = *initPlace++;
            *str++ = '-';
        }
        /* a sequence of 2 - copy both letters */
        else if (i - initPlace > 0){
            *str++ = *initPlace++;
        }
        *str++ = *i++;
    }
    /* make sure string ends with \0 */
    *str = '\0';
}

int main(){
	/*Asks the user for input, runs abc and prints the string*/
    char str[STR_SIZE];
    char *endLine; /* placeholder for the \n in the end of line to change to \0 */
    while(TRUE){
		printf("Please enter a string of up to 80 chars and then hit enter (empty line to exit):\n");
		fgets(str, STR_SIZE, stdin);
        /* replace \n in end of line with \0*/
		endLine = strchr(str, '\n');
        *endLine = '\0';
        /* Break if no input was entered*/
		if(! *str){
			break;
		}
		printf("%s\n", str);
		abc(str);
		printf("%s\n", str);
    }
    return 0;
}
