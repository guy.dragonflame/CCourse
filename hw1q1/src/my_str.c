/*
 Answer to HW1 Q1: get a char (ch) and a string (str) from the user, print all the suffixes of the char in
 the string and then the number of suffixes found.
 I assume all the data is valid (as instructed in the question).
 */

#include <stdio.h>
#include <string.h>
#define STR_SIZE 82 /* uses 82 to save place for \n\0 in the end. \n will be replaced with \0*/
#define TRUE 1


int suffix(char *str, char ch){
    /* Prints all the suffixes of the ch char in the str string and returns the number of suffixes found. */
    int suff_counter = 0;
    /* Go over from one ch to the other, each time incrementing suff_counters*/
    for (; (str=strchr(str, ch)); suff_counter++){
        printf("%s\n", str++);
    }
    return suff_counter;
}


int main(){
	/*Asks the user for input, runs suffix and prints the number of suffixes found*/
    char ch, str[STR_SIZE];
    char *endLine; /* placeholder for the \n in the end of line to change to \0 */
    while(TRUE){
        printf("please enter a cahr (enter new line to quit)\n");
        ch = getchar();
        if (ch == '\n'){
        	break;
        }
        getchar(); /* in order to prevent fgets to use the newline*/
        printf("%c\n", ch);
        printf("please enter a string of up to 80 chars\n");
        fgets(str, STR_SIZE, stdin);
        /* replace \n in end of line with \0*/
		endLine = strchr(str, '\n');
        *endLine = '\0';
        printf("%s\n\n", str);
        printf ("Found %d suffixes\n", suffix(str, ch));

    }
    return 0;
}
